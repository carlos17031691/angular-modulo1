import { Component, OnInit, Input } from '@angular/core';
import { Vehiculo } from '../../models/vehiculo.model';


@Component({
  selector: 'app-whitelist',
  templateUrl: './whitelist.component.html',
  styleUrls: ['./whitelist.component.css']
})
export class WhitelistComponent implements OnInit {
  @Input() vehiculo: Vehiculo;
  constructor() { 

  }

  ngOnInit(): void {
  }

}
