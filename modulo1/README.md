# Modulo 1 Angular

Este proyecto contiene la evaluación del modulo 1 del curso de Angular

## Contenido

1. el paquete npm de Bootstrap instalado.
2. la importación de los css de bootstrap en los estilos globales de la app angular.
3. el desarrollo de al menos un componente donde se utilicen los estilos de Bootstrap
4. al menos en un componente, la sintaxis {{ }} para cargar al menos un dato de alguna variable Typescript, por ejemplo, nombre o descripción.
5. un componente contenedor de un listado de objetos, en el cual se vea un array de elementos, y éstos deben verse en el navegador.
6. en la plantilla html del listado, un tag UL con elementos LI, y con uso de la directiva ngfor, que esté siendo usada para iterar sobre los elementos a mostrar.
7. el uso de @HostBinding en al menos un componente
8. en el componente de listado, un formulario html con resoluciones de angular (es decir las variables de plantilla con #), que sea utilizado para ingresar los datos
9. que con la sintaxis de variables de formulario con el token #, al hacer click en un botón de submit, se invoque a una función del componente en typescript, con el fin de agregar un nuevo ítem al listado
10. en el componente contenedor del listado, que en typescript exista la función en la cual se agregue al array de elementos a mostrar al nuevo elemento, y que, como consecuencia, y de manera reactiva, se actualice la interfaz de usuario.


## realizado por Carlos Torres