export class Vehiculo {
    nombre: string;
    imageUrl: string;

    constructor(nombre, url) {
        this.nombre = nombre;
        this.imageUrl = url;
    }
}