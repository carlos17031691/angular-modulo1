import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { VehiculosComponent } from './vehiculos/vehiculos.component';
import { WhitelistComponent } from './vehiculos/whitelist/whitelist.component';

@NgModule({
  declarations: [
    AppComponent,
    VehiculosComponent,
    WhitelistComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
