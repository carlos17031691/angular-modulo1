import { Component, OnInit } from '@angular/core';
import { Vehiculo } from '../models/vehiculo.model'

@Component({
  selector: 'app-vehiculos',
  templateUrl: './vehiculos.component.html',
  styleUrls: ['./vehiculos.component.css']
})
export class VehiculosComponent implements OnInit {
  vehiculos: Vehiculo[];
  constructor() { 
    this.vehiculos = []
  }

  ngOnInit(): void {
  }

  guardar(nombre: string,url: string) {
    this.vehiculos.push(new Vehiculo(nombre,url))
    console.log(nombre, url)
  }

}
